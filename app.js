const recipeTitles = $('.recipe-titles');
const addedRecipes = [];

$(appReady);

function appReady() {
  $('form').submit(submitForm);
}

const submitForm = event => {
  event.preventDefault();
  const searchTerm = $('#search').val();

  searchForRecipes(searchTerm)
    .then(showRecipes);
}

const searchForRecipes = searchTerm => {
  const PROXY_URL = 'https://galvanize-cors-proxy.herokuapp.com/';
  const API_URL = `${PROXY_URL}http://www.recipepuppy.com/api/?q=${searchTerm}`;
  return $.getJSON(API_URL).then(result => result.results);
}

const showRecipes = recipes => {
  recipes.forEach(addRecipe);
}

const addRecipe = recipe => {
  const recipeTitle = $(`<h3 data-json='${JSON.stringify(recipe)}'>${recipe.title}</h3>`);
  recipeTitle.click(recipeTitleClicked);
  recipeTitles.append(recipeTitle);
}

function recipeTitleClicked() {
  const recipe = JSON.parse(this.dataset.json);
  addedRecipes.push(recipe);
  $('.added-recipes').append(`
    <h4>${recipe.title}</h4>
    <img src="${recipe.thumbnail}">
    <p>${recipe.ingredients}</p>
    <a href="${recipe.href}" target="_blank">View Recipe</a>
  `)
}
